import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native';

export default StyleSheet.create({
  employeeItem: {
    flexDirection: 'row',
    padding: 10,
    paddingVertical: 15,
  },
  employeeAvatar: {
    height: 48,
    width: 48,
    borderWidth: 1,
    borderRadius: 100,
    marginHorizontal: 10,
  },
  employeeInfo: {
    justifyContent: 'space-around',
  },
  container: {
    backgroundColor: "#DBF1FD",
    width: Dimensions.get("window").width / 2,
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 10,
    flex: 1,
    alignSelf: "center",
    alignItems: "center"
  },
  avatar: {
    height: 60,
    width: 60,
    backgroundColor: "#808080",
    borderRadius: 30,
    paddingTop: 20,
    marginTop: 20,
    alignItems: "center",
  },
  avatarKeyword: {
    fontWeight: "bold",
  },
  userInfo: {
    flex: 1,
    width: "100%",
    padding: 20,
  },
  name: {
    fontWeight: "bold",
  },
});
